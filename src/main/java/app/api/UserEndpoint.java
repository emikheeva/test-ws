package app.api;

import app.exception.UserServiceException;
import app.logic.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import test_ws.users.AddUserRequest;
import test_ws.users.GetUserRequest;
import test_ws.users.GetUserResponse;


@Endpoint
public class UserEndpoint {
    private static final String NAMESPACE_URI = "http://test-ws/users";

    private UserService userService;

    @Autowired
    public UserEndpoint(UserService userService) {
        this.userService = userService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUserRequest")
    @ResponsePayload
    public GetUserResponse getUser(@RequestPayload GetUserRequest request) {
        GetUserResponse response = new GetUserResponse();
        response.getUser().addAll(userService.getUser(request.getFirstname(), request.getLastname()
                , request.getEmail(), request.getBirthdate()));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addUserRequest")
    @ResponsePayload
    public void addUser(@RequestPayload AddUserRequest request) throws UserServiceException {
        userService.addUser(request.getUser());
    }
}
