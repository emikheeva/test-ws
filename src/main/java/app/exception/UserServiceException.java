package app.exception;

public class UserServiceException extends Exception {

    public UserServiceException(String message){
        super(message);
    }

    public static class UserAlreadyExistsException extends UserServiceException{

        public UserAlreadyExistsException() {
            super("User already exists");
        }
    }
}
