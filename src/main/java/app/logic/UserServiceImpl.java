package app.logic;

import app.exception.UserServiceException;
import org.springframework.stereotype.Service;
import test_ws.users.User;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final List<User> users = new ArrayList<>();

    @Override
    public synchronized void addUser(User user) throws UserServiceException {
        List<User> duplicate = getUser(user.getFirstname(), user.getLastname()
                , user.getEmail(), user.getBirthdate());
        if (duplicate.isEmpty())
                users.add(user);
        else
            throw new UserServiceException.UserAlreadyExistsException();
    }

    @Override
    public List<User> getUser(String firstname, String lastname, String email, XMLGregorianCalendar birthdate){
        return users.stream().filter(u ->
            (firstname == null || firstname != null && u.getFirstname().equals(firstname))
                    && (lastname == null || lastname != null && u.getLastname().equals(lastname))
                    && (email == null || email != null && u.getEmail().equals(email))
                    && (birthdate == null || birthdate != null && u.getBirthdate().getYear() == birthdate.getYear()
                    && u.getBirthdate().getMonth() == birthdate.getMonth()
                    && u.getBirthdate().getDay() == birthdate.getDay()))
                .collect(Collectors.toList());
    }
}
