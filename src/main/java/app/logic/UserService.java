package app.logic;

import app.exception.UserServiceException;
import test_ws.users.User;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.List;


public interface UserService {

    /**
     * Adds new user to collection.
     * Throws UserServiceException.UserAlreadyExistsException if user with same parameters already exists.
     * @param user
     * @throws UserServiceException
     */
    public void addUser(User user) throws UserServiceException;

    /**
     * Returns list of users with parameters.
     * @param firstname
     * @param lastname
     * @param email
     * @param birthdate
     * @return
     */
    public List<User> getUser(String firstname, String lastname, String email, XMLGregorianCalendar birthdate);
}
