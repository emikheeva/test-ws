import app.logic.UserService;
import app.logic.UserServiceImpl;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public abstract class AbstractServiceTest {

    @Configuration
    static class ContextConfiguration {

        @Bean
        public UserService userService() {
            UserService userService = new UserServiceImpl();
            return userService;
        }
    }
}

