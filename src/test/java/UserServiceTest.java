import app.logic.UserService;
import app.exception.UserServiceException;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test_ws.users.User;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class UserServiceTest extends AbstractServiceTest {
    @Autowired
    UserService userService;

    @Test
    public void addUserTest() {
        //add new user
        User user = getSampleUser();
        try {
            userService.addUser(user);
        }
        catch (UserServiceException ex){}

        List<User> users = userService.getUser(user.getFirstname(), user.getLastname()
                , user.getEmail(), user.getBirthdate());
        Assert.assertEquals(users.size(), 1);

        //trying to add duplicate user
        user = getSampleUser();
        try {
            userService.addUser(user);
        }
        catch (UserServiceException ex){}

        users = userService.getUser(user.getFirstname(), user.getLastname()
                , user.getEmail(), user.getBirthdate());
        Assert.assertEquals(users.size(), 1);

        //add new user with same parameters, except email
        user = getSampleUser();
        user.setEmail("lola1@gmail.com");
        try {
            userService.addUser(user);
        }
        catch (UserServiceException ex){}

        users = userService.getUser(user.getFirstname(), user.getLastname()
                , user.getEmail(), user.getBirthdate());
        Assert.assertEquals(users.size(), 1);

        //get users with same firstname, lastname, birthdate
        users = userService.getUser(user.getFirstname(), user.getLastname(), null, user.getBirthdate());
        Assert.assertEquals(users.size(), 2);

        //get all users
        users = userService.getUser(null, null, null, null);
        Assert.assertEquals(users.size(), 2);
    }

    private User getSampleUser(){
        User user = new User();
        user.setFirstname("Lola");
        user.setLastname("Spring");
        user.setEmail("lola@gmail.com");
        GregorianCalendar c = new GregorianCalendar();
        c.set(Calendar.YEAR, 1987);
        c.set(Calendar.MONTH, 10);
        c.set(Calendar.DAY_OF_MONTH, 24);
        try {
            XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            user.setBirthdate(date);
        }
        catch (DatatypeConfigurationException ex){}

        return user;
    }

}